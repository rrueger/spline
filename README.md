# Parabola/Arch Linux example install that connects the dots.

Hence "spline".

The Archwiki is great, but sometimes it can be difficult to get the big picture
of what needs to be done when combining "non-standard" features. This script
serves as an example.

PLEASE READ THE FULL OFFICIAL INSTALL GUIDE FIRST.

First and foremost, this is a way to document and streamline my Arch/Parabola
install, so that if I ever need to re-install, I know exactly what I did.
The rest is just some syntactic sugar to make interacting with the script more
comfortable.

This _really_ is just an example, and in no way reflects best practices.

It _definitively_ should not just be 'copy and pasted'.

It just gives a working example of how one *could* install arch.

PROs of this install method:
- LVM on LUKS. Root, boot, swap and home all reside on the same physical
  partition and are all encrypted with in one LUKS container.
- Encrypted boot. This prevents kernel tampering.
- Only asks for LUKS passphrase once in GRUB owing to the creation of the
  `crypto_keyfile.bin` file.

CONS of this install method:
- Uses older `luks1` instead of `luks2`. This is needed for GRUB to access
  encrypted boot as GRUB does not yet support `luks2`. [bug #55093](https://savannah.gnu.org/bugs/?55093)

Note: to download this file on a system that doesn't have `git` installed
yet, use the `raw` version of this file which can be downloaded using `curl` or
`wget`.

`$ wget https://gitlab.ethz.ch/rrueger/spline/raw/master/spline.sh`

This is also why the `README` is included in the script.
