#!/bin/bash
# 2019 Velleto
# LICENCE: Affero GNU Public License


################################################################
# README
################################################################

# Parabola/Arch Linux example install that connects the dots.
# Hence "spline".

# The Archwiki is great, but sometimes it can be difficult to get the big
# picture of what needs to be done when combining "non-standard" features. This
# script serves as an example.

# PLEASE READ THE FULL OFFICIAL INSTALL GUIDE FIRST.

# First and foremost, this is a way to document and streamline my Arch/Parabola
# install, so that if I ever need to re-install, I know exactly what I did.
# The rest is just some syntactic sugar to make interacting with the script more
# comfortable.

# This _really_ is just an example, and in no way reflects best practices.

# It _definitively_ should not just be 'copy and pasted'.

# It just gives a working example of how one *could* install arch.

# PROs of this install method:
# - LVM on LUKS. Root, boot, swap and home all reside on the same physical
#   partition and are all encrypted with in one LUKS container.
# - Encrypted boot. This prevents kernel tampering.
# - Only asks for LUKS passphrase once in GRUB owing to the creation of the
#   `crypto_keyfile.bin` file.

# CONS of this install method:
# - Uses older `luks1` instead of `luks2`. This is needed for GRUB to access
#   encrypted boot as GRUB does not yet support `luks2`.
#   [bug #55093](https://savannah.gnu.org/bugs/?55093)

# Note: to download this file on a system that doesn't have `git` installed yet,
# use the `raw` version of this file which can be downloaded using `curl` or
# `wget`.

# `$ wget https://gitlab.ethz.ch/rrueger/spline/raw/master/spline.sh`

################################################################

set -e

################################################################
# CONFIGURE
################################################################
# Disks
DISK="/dev/nvme0n1"
EFI="/dev/nvme0n1p1"
MAIN="/dev/nvme0n1p2"
SWAPSIZE="16G"
ITERATIONS="200000"
# LVM
PV_NAME="crypt"
LV_NAME="vg"
# Configs
LOCALE="en_GB.UTF-8"
LOCALE_LONG="en_GB.UTF-8 UTF-8"
KEYMAP="uk"
HOST="laptop's name"
USERNAME="my name"
USERSHELL="zsh"
USERID="60000 > favourite number > 1000"
FILESYSTEM="xfs"
################################################################

help () {
    echo "Usage:"
    echo "There are 13 stages to this install script."
    echo "They have been aliased into 3 rough sections (4 commands) to make the"
    echo "install a bit more hands-free."
    echo
    echo "DON'T FORGET TO CONFIGURE SETTINGS IN THIS FILE."
    echo "FAILURE TO DO SO MAY CAUSE SYSTEM FAILURE AND DATA LOSS."
    echo
    echo "The sections go:"
    echo
    echo "  (1) install   - Disk paritioning and installing the base system"
    echo "  (2) configure - Bootloader, mkinitcpioinfo and crypto keyfile."
    echo "  (3) setup     - User accounts, desktop environments"
    echo
    echo "Main commands:"
    echo
    echo "  (1) $ bash spline.sh install"
    echo "  (2) $ arch-chroot /mnt"
    echo "  (3) $ bash spline.sh configure"
    echo
    echo "  Now the base install is done. You can complete this step later."
    echo
    echo "  (4) $ bash spline.sh setup"
    echo
    echo "In any case, exit chroot and unmount partitions before rebooting."
    echo
    echo "    $ exit"
    echo "    $ umount /mnt/{efi,}"
    echo "    $ reboot"
    echo
    echo
    echo "For a more granular control, use individual sections"
    echo "i.e. bash spline.sh <n>."
    echo
    echo "If any errors are encountered after stage 3 and the disks need to be"
    echo "unlocked and remounted, use \"$ bash spline.sh mount\" to mount the"
    echo "disks correctly."
}

usage () {
    echo "Usage: bash spline.sh <n>"
    echo "OPTIONS"
    echo "-h        For more help."
    sections
}

sections () {
    echo "alias: \"install\" for sections 1-5."
    echo " 1) (install) Set Localisation (time and keyboard for install)"
    echo " 2) (install) Prepare disks (Parition, encrypt, create LVs, create filesytems)"
    echo " 3) (install) Mount disks and enable swap"
    echo " 4) (install) Update Pacman Keys"
    echo " 5) (install) Install base, with some system tools"
    echo
    echo
    echo "(Inside Chroot)"
    echo "alias: \"configure\" for sections 6-10."
    echo " 6) (configure) Set Localisation (time, keyboard, locales)"
    echo " 7) (configure) Set hostname and configure hosts"
    echo " 8) (configure) Add keyfile to encrypted system"
    echo " 9) (configure) Add Mkinitcpio HOOKS and make"
    echo "10) (configure) Configure GRUB bootloader with cryptdevice config"
    echo
    echo
    echo "(Inside Chroot)"
    echo "alias: \"setup\" for sections 11-13."
    echo "11) (setup) Basic configuration (resolv.conf, set root passwd, create default user)"
    echo "12) (setup) Update system (Configure Pacman use Reflector)"
    echo "13) (setup) Install Desktop Environment (KDE plasma, SDDM)"
    echo
    echo
    echo "mount) Unlock and mount disks."
}

sanity_check () {
    set +x
    echo "--------------------------------"
    echo "PLESE READ THIS CAREFULLY!"
    echo "--------------------------------"
    echo "Your current configurations are:"
    echo -e "Main disk:                   $DISK"
    echo -e "System Partition (Encypted): $MAIN"
    echo -e "EFI Parition:                $EFI"
    echo -e "EFI Mout point:              /efi"
    echo -e "SWAP size:                   $SWAPSIZE"
    echo
    echo -e "Hostname:                    $HOST"
    echo -e "Username:                    $USERNAME"
    echo -e "Userid:                      $USERID"
    echo -e "Shell:                       $USERSHELL"
    echo "Press enter to continue..."
    read -r
}

install_set_localisation () {
    set -x
    # Keymap
    loadkeys uk

    # Set time
    timedatectl set-ntp true
    ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime
}

install_prepare_disks () {
    set +x
    echo "Create:"
    echo
    echo "$EFI:    100M, EFI System."
    echo "$MAIN:   Remainder, Linux Filesystem."
    echo
    echo "Press Enter to continue"
    read -r
    set -x

    cfdisk "$DISK"

    # Encrypt Main Partition
    cryptsetup                                 \
        --cipher aes-xts-plain64               \
        --key-size 512                         \
        --hash sha512                          \
        --pbkdf-force-iterations "$ITERATIONS" \
        --use-urandom                          \
        --verify-passphrase                    \
        --type luks1                           \
        luksFormat "$MAIN"

    cryptsetup open "$MAIN" "$PV_NAME"
    pvcreate /dev/mapper/"$PV_NAME"
    vgcreate $LV_NAME /dev/mapper/"$PV_NAME"
    lvcreate -L $SWAPSIZE $LV_NAME -n swap
    lvcreate -l 100%FREE $LV_NAME -n root

    mkfs."$FILESYSTEM" /dev/$LV_NAME/root
    mkswap /dev/$LV_NAME/swap

    # Prepare EFI boot partition
    mkfs.fat -F32 "$EFI"
}

install_mount_disks () {
    set -x
    mount /dev/$LV_NAME/root /mnt
    swapon /dev/$LV_NAME/swap

    mkdir -p /mnt/efi
    mount "$EFI" /mnt/efi
}

install_mount_disks () {
    set -x
    cryptsetup open "$MAIN" "$PV_NAME"
    sleep 3

    install_mount_disks
}

install_update_keys () {
    set -x
    # Update keys
    pacman-key --refresh-keys
}

install_base () {
    set -x

    # Show overall download stats, not per package.
    sed -i 's|\[options\]|[options]\nTotalDownload|' /etc/pacman.conf

    # Install the base system, with some extra tools.
    # This list is probably a little generous.
    # The second half is a list of packages that used to be in base
    # but were removed in the name of slimming down arch installations
    pacstrap /mnt base linux base-devel \
        cron                            \
        dialog                          \
        git                             \
        grub                            \
        networkmanager                  \
        sudo                            \
        vim                             \
        wget                            \
        wpa_supplicant                  \
        pacman-contrib                  \
        $USERSHELL                      \
                                        \
        cryptsetup                      \
        device-mapper                   \
        dhcpcd                          \
        diffutils                       \
        e2fsprogs                       \
        inetutils                       \
        jfsutils                        \
        less                            \
        linux-firmware                  \
        logrotate                       \
        lvm2                            \
        man-db                          \
        man-pages                       \
        mdadm                           \
        nano                            \
        netctl                          \
        perl                            \
        reiserfsprogs                   \
        s-nail                          \
        sysfsutils                      \
        systemd-sysvcompat              \
        texinfo                         \
        usbutils                        \
        vi                              \
        which                           \
        xfsprogs                        \

    genfstab -U -p /mnt >> /mnt/etc/fstab

    # Copy installer into chroot environment.
    cp spline.sh /mnt
}

chroot_set_localisation () {
    set -x

    # Set times and Locales.
    ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime
    hwclock --systohc
    sed -i "s|#$LOCALE_LONG|$LOCALE_LONG|" /etc/locale.gen
    locale-gen
    echo "LANG=$LOCALE" > /etc/locale.conf
    export "LANG=$LOCALE"
    echo "KEYMAP=$KEYMAP" > /etc/vconsole.conf
}

chroot_hostnames () {
    set -x
    # Set hostnames
    echo "$HOST" > /etc/hostname
    echo -e "127.0.0.1\t\tlocalhost\n::1\t\t\tlocalhost\n127.0.1.1\t\t$HOST.localdomain\t$HOST" | sed -e 's|^\s*||' > /etc/hosts
}

chroot_add_keyfile () {
    set -x

    # So the passphrase only needs to be entered at GRUB once.
    dd if=/dev/urandom of=/crypto_keyfile.bin bs=512 count=16 iflag=fullblock
    chmod 000 /crypto_keyfile.bin
    cryptsetup luksAddKey "$MAIN" /crypto_keyfile.bin --pbkdf-force-iterations "$ITERATIONS"

    # Tell mkinitcpio where the keyfile is.
    sed -i 's|^FILES=.*|FILES=(/crypto_keyfile.bin)|' /etc/mkinitcpio.conf
}

chroot_mkinitcpio () {
    set -x

    MKINITCPIO_HOOKS="base udev autodetect keyboard keymap modconf block encrypt lvm2 resume filesystems fsck"

    sed -i "s|^HOOKS=.*|HOOKS=($MKINITCPIO_HOOKS)|" /etc/mkinitcpio.conf

    mkinitcpio -p linux
}

chroot_grub () {
    set -x

    # Not sure if _all_ are necessary.
    pacman --noconfirm --needed -S grub efibootmgr dosfstools os-prober mtools

    # Can also specify manually with UUID
    KERNEL_OPTIONS="quiet cryptdevice=$MAIN:$LV_NAME root=/dev/mapper/$LV_NAME-root resume=/dev/mapper/$LV_NAME-swap"

    sed -i                                                                                    \
        -e "s|^GRUB_CMDLINE_LINUX_DEFAULT=.*|GRUB_CMDLINE_LINUX_DEFAULT=\"$KERNEL_OPTIONS\"|" \
        -e 's|^#GRUB_ENABLE_CRYPTODISK=.*|GRUB_ENABLE_CRYPTODISK=y|'                          \
        /etc/default/grub

    # Not sure if this env var is needed
    export esp="/efi"

    grub-install                  \
        --target=x86_64-efi       \
        --efi-directory="$esp"    \
        --bootloader-id=grub_uefi \
        --recheck

    grub-mkconfig -o /boot/grub/grub.cfg
}

setup_user () {
    set -x

    # Network
    # Not sure if this is needed
    # echo -e "nameserver\t192.168.0.1" >> /etc/resolv.conf

    # Set root password
    echo "Set password for root."
    passwd

    echo "Creating new user \"$USERNAME\""
    useradd -u "$USERID" -m "$USERNAME"
    passwd "$USERNAME"
    chsh -s "$(command -v $USERSHELL)" "$USERNAME"

    # Add user to sudoers
    sed -i "s|root ALL=(ALL) ALL|&\n$USERNAME ALL=(ALL) ALL, NOPASSWD: /bin/xbacklight, /bin/bluetooth, /bin/wifi|" /etc/sudoers
}

setup_update () {
    set -x
    sed -i 's|\[options\]|&\nTotalDownload\nColor\nVerbosePkgLists\n|' /etc/pacman.conf
    pacman --noconfirm --needed -S reflector
    # Find fastest mirrors
    reflector --latest 200 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist
    pacman --noconfirm --needed -Syyuu
}

setup_desktop_environment () {
    set -x

    pacman --noconfirm --needed -S \
        tlp                        \
        NetworkManager             \
        xdm                        \
        i3-gaps

    systemctl enable tlp
    systemctl enable xdm
    systemctl enable NetworkManager

    # Randomise MAC Addresses
    printf \
    '
    [device-mac-randomization]
    wifi.scan-rand-mac-address=yes
    [connection-mac-randomization]
    ethernet.cloned-mac-address=random
    wifi.cloned-mac-address=random
    ' | sed -e 's|^\s*||' -e '/^$/d' > /etc/NetworkManager/NetworkManager.conf

    # Handle power key properly
    sed -E \
        -e 's|^[#]{0,1}HandlePowerKey.*|HandlePowerKey=ignore|' \
        -e 's|^[#]{0,1}HandleSuspendKey.*|HandleSuspendKey=ignore|' \
        -e 's|^[#]{0,1}HandleHibernateKey.*|HandleHibernateKey=ignore|' \
        -i /etc/systemd/logind.conf \

}


case $1 in
    1)                 install_set_localisation  ;;
    2)  sanity_check ; install_prepare_disks     ;;
    3)  sanity_check ; install_mount_disks       ;;
    4)                 install_update_keys       ;;
    5)                 install_base              ;;

    # In chroot environment
    6)                 chroot_set_localisation   ;;
    7)  sanity_check ; chroot_hostnames          ;;
    8)  sanity_check ; chroot_add_keyfile        ;;
    9)  sanity_check ; chroot_mkinitcpio         ;;
    10) sanity_check ; chroot_grub               ;;

    # After install
    11) sanity_check ; setup_user                ;;
    12)                setup_update              ;;
    13)                setup_desktop_environment ;;

    mount)
        mount_disks
        ;;

    install)
        sanity_check
        install_set_localisation
        install_prepare_disks
        install_mount_disks
        install_update_keys
        install_base
        ;;

    configure)
        sanity_check
        chroot_set_localisation
        chroot_hostnames
        chroot_add_keyfile
        chroot_mkinitcpio
        chroot_grub
        ;;

    setup)
        sanity_check
        setup_user
        setup_update
        setup_desktop_environment
        ;;

    -h) help | less
        ;;

    *) usage
        ;;
esac

# Post Install Notes:
# Go to BIOS settings, Add boot option, FS0:/EFI/grub_uefi/grubx64.efi
